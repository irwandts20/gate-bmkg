package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

//Fault is struct for fault.xml
type Fault struct {
	XMLName     xml.Name `xml:"fault"`
	Code        string   `xml:"code"`
	Message     string   `xml:"message"`
	Description string   `xml:"description"`
}

//Fault is struct for fault.json
type FaultJ struct {
	Code        int    `json:"code"`
	Message     string `json:"message"`
	Description string `json:"description"`
}

//ErrorResponse struct to handle error
type ErrorResponse struct {
	FaultObj Fault `json:"fault"`
}

func main() {
	xmlFile, err := os.Open("fault.xml")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("opening file success")

	defer xmlFile.Close()

	byteValue, _ := ioutil.ReadAll(xmlFile)

	var fault Fault
	xml.Unmarshal(byteValue, &fault)

	fmt.Println("done unmarshelling")
	fmt.Println(fault)

	jsonFile, err := os.Open("fault.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("opening file success")

	defer jsonFile.Close()

	byteValue1, _ := ioutil.ReadAll(jsonFile)

	var errorr ErrorResponse

	json.Unmarshal(byteValue1, &errorr)

	fmt.Println("done unmarshelling")
	fmt.Println(errorr)
	fmt.Print("code : ")
	fmt.Println(errorr.FaultObj.Code)

	fmt.Print("message : ")
	fmt.Println(errorr.FaultObj.Message)

	fmt.Print("description : ")
	fmt.Println(errorr.FaultObj.Description)

}
